<?php


namespace Tests\Unit\Http\Services;

use App\DTO\RepaymentCalculatorDTO;
use App\DTO\RepaymentsFrequency;
use App\Http\Services\RepaymentCalculatorService;
use JetBrains\PhpStorm\ArrayShape;
use PHPUnit\Framework\TestCase;

class RepaymentCalculatorServiceTest extends TestCase
{
    /**
     * Tests if the calculations are correct.
     *  @dataProvider validDataProvider
     * @param RepaymentCalculatorDTO $repaymentCalculatorDTO
     * @param float $expectedResult
     * @throws \Exception
     */
    public function testCalculateRepayments(RepaymentCalculatorDTO $repaymentCalculatorDTO, float $expectedResult): void
    {
        $repaymentCalculatorService = new RepaymentCalculatorService();
        $result = $repaymentCalculatorService->calculateRepayments($repaymentCalculatorDTO);
        self::assertEquals($expectedResult, $result);
    }

    /**
     * Data provider to verify if the calculations are correct.
     * @return array[]
     */
    #[ArrayShape([
        'Monthly repayments with 5% rate for 5 years for $50000' => "array",
        'Monthly repayments with 5% rate for 3 years for $50000' => "array",
        'Monthly repayments with 5% rate for 1 year for $50000' => "array",
        'Weekly repayments with 5% rate for 5 years for $50000' => "array",
        'Weekly repayments with 5% rate for 3 years for $50000' => "array",
        'Weekly repayments with 5% rate for 1 year for $50000' => "array",
        'Fortnightly repayments with 5% rate for 5 years for $50000' => "array",
        'Fortnightly repayments with 5% rate for 3 years for $50000' => "array",
        'Fortnightly repayments with 5% rate for 1 year for $50000' => "array"
    ])] public function validDataProvider(): array
    {
        return [
            'Monthly repayments with 5% rate for 5 years for $50000' => [
                # TEST data for monthly repayments
                'DTO' => new RepaymentCalculatorDTO(
                    50000,
                    5,
                    5,
                    RepaymentsFrequency::from('monthly')
                ),
                'expectedResult' => 1041.67,
            ],
            'Monthly repayments with 5% rate for 3 years for $50000' => [
                'DTO' => new RepaymentCalculatorDTO(
                    50000,
                    5,
                    3,
                    RepaymentsFrequency::from('monthly')
                ),
                'expectedResult' => 1597.22,
            ],
            'Monthly repayments with 5% rate for 1 year for $50000' => [
                'DTO' => new RepaymentCalculatorDTO(
                    50000,
                    5,
                    1,
                    RepaymentsFrequency::from('monthly')
                ),
                'expectedResult' => 4375,
            ],
            'Weekly repayments with 5% rate for 5 years for $50000' => [
                # TEST data for weekly repayments
                'DTO' => new RepaymentCalculatorDTO(
                    50000,
                    5,
                    5,
                    RepaymentsFrequency::from('weekly')
                ),
                'expectedResult' => 239.46,
            ],
            'Weekly repayments with 5% rate for 3 years for $50000' => [
                'DTO' => new RepaymentCalculatorDTO(
                    50000,
                    5,
                    3,
                    RepaymentsFrequency::from('weekly')
                ),
                'expectedResult' => 368.59,
            ],
            'Weekly repayments with 5% rate for 1 year for $50000' => [
                'DTO' => new RepaymentCalculatorDTO(
                    50000,
                    5,
                    1,
                    RepaymentsFrequency::from('weekly')
                ),
                'expectedResult' => 1009.62,
            ],
            'Fortnightly repayments with 5% rate for 5 years for $50000' => [
                # TEST data for fortnightly repayments
                'DTO' => new RepaymentCalculatorDTO(
                    50000,
                    5,
                    5,
                    RepaymentsFrequency::from('fortnightly')
                ),
                'expectedResult' => 478.93,
            ],
            'Fortnightly repayments with 5% rate for 3 years for $50000' =>[
                'DTO' => new RepaymentCalculatorDTO(
                    50000,
                    5,
                    3,
                    RepaymentsFrequency::from('fortnightly')
                ),
                'expectedResult' => 737.18,
            ],
            'Fortnightly repayments with 5% rate for 1 year for $50000' =>[
                'DTO' => new RepaymentCalculatorDTO(
                    50000,
                    5,
                    1,
                    RepaymentsFrequency::from('fortnightly')
                ),
                'expectedResult' => 2019.23,
            ]
        ];
    }
}
