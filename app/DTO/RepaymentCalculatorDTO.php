<?php


namespace App\DTO;

use Spatie\LaravelData\Attributes\Validation\GreaterThan;
use Spatie\LaravelData\Attributes\WithCast;
use Spatie\LaravelData\Casts\EnumCast;
use Spatie\LaravelData\Data;

class RepaymentCalculatorDTO extends Data
{
    public function __construct(
        #[GreaterThan(0)]
        public float $borrowedAmount,
        #[GreaterThan(0)]
        public float $interestRate,
        #[GreaterThan(0)]
        public int $tenureInYears,
        #[WithCast(EnumCast::class)]
        public RepaymentsFrequency $repaymentFrequency
    ) {
    }
}
