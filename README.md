# Money.com.au Developer Challenge

####Using the code included in this repository, complete the below task:

Using the foundational code that has been included in this repository, update the calculator that can be found at the index
route of this project to be functional. 

The calculator is a simple interest calculator, using an amount, term and interest rate to calculate a repayment amount. 

Once the rate has been calculated, please display in the selected frequency (Default to 'Month').

You can find the foundational work already done in:
- app/Http/Livewire/ExampleCalculator.php
- resources/views/livewire/example-calculator.blade.php

### Steps:

1. Fork this repo. 
2. Complete the above task. 
3. Upload your changes, and alert your contact at money.com.au that you are ready for review.

### What you will be assessed on: 

1. Functionality - The most important factor will be if your solution works, and the math is accurate. 
2. Code Cleanliness, consistency, type hinting and good OOP principals are ideals. 
3. Understanding of Laravel and Livewire functionality - feel free to show off. 

**NOTE: The UI is not important, but extra credit *if* you want to make any visual changes.**

## Good Luck, and have fun! 
