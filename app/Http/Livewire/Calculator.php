<?php

namespace App\Http\Livewire;

use App\DTO\RepaymentCalculatorDTO;
use App\DTO\RepaymentsFrequency;
use App\Http\Services\RepaymentCalculatorServiceInterface;
use Livewire\Component;
use \Illuminate\Contracts\View\Factory;
use \Illuminate\Contracts\View\View;
use Illuminate\Contracts\Foundation\Application;

class Calculator extends Component
{
    /**
     * @var float
     */
    public float $result = 0.00;

    /**
     * @var float
     */
    public float $loanAmount = 0;

    /**
     * @var float
     */
    public float $interestRate = 0;

    /**
     * @var int
     */
    public int $termDuration = 1;

    /**
     * @var string
     */
    public string $repaymentFrequency = 'monthly';

    /**
     * @var RepaymentCalculatorServiceInterface
     */
    private RepaymentCalculatorServiceInterface $repaymentCalculator;

    public array $frequencyOptions = [
        'monthly',
        'fortnightly',
        'weekly'
    ];

    /**
     * Validation rules for the user data in this component.
     * @var array|string[]
     */
    protected array $rules = [
        'loanAmount' => 'required|numeric|gt:0',
        'interestRate' => 'required|numeric|gt:0',
        'termDuration' => 'in:1,3,5',
        'repaymentFrequency' => 'in:monthly,fortnightly,weekly'
    ];

    /**
     * Life cycle event
     * @param RepaymentCalculatorServiceInterface $repaymentCalculator
     */
    public function boot(RepaymentCalculatorServiceInterface $repaymentCalculator): void
    {
        # INJECT repayment calculate service as a dependency.
        $this->repaymentCalculator = $repaymentCalculator;
    }

    /**
     * Calculate Repayments
     */
    public function calculateRepayments(): void
    {
        # VALIDATE the data.
        $this->validate();

        # CREATING a data transfer object does not make sense here after all the validation but this will ensure that
        # the repayments calculation service class always receive valid data for calculations wherever it is used.
        $repaymentsCalculatorDTO = RepaymentCalculatorDTO::from([
                'borrowedAmount' => $this->loanAmount,
                'interestRate' => $this->interestRate,
                'tenureInYears' => $this->termDuration,
                'repaymentFrequency' => RepaymentsFrequency::from($this->repaymentFrequency)
            ]);
        $this->result = $this->repaymentCalculator->calculateRepayments(
            $repaymentsCalculatorDTO
        );
    }

    /**
     *  Calculate repayments when anything is updated.
     */
    public function updated(): void
    {
        $this->calculateRepayments();
    }

    /**
     * Render the component vew.
     * @return Application|Factory|View
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.calculator');
    }

    /**
     * Recalculate payments everytime frequency is updated from the view.
     * @param $repaymentFrequency
     */
    public function updateRepaymentsFrequency($repaymentFrequency): void
    {
        $this->repaymentFrequency = $repaymentFrequency;
        $this->calculateRepayments();
    }
}
