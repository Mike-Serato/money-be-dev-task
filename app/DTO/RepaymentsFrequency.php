<?php


namespace App\DTO;

use Spatie\Enum\Enum;

/**
 * @method static self monthly()
 * @method static self weekly()
 * @method static self fortnightly()
 */
class RepaymentsFrequency extends Enum
{

}
