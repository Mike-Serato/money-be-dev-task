<?php

namespace App\Providers;

use App\Http\Services\RepaymentCalculatorService;
use App\Http\Services\RepaymentCalculatorServiceInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(
            RepaymentCalculatorServiceInterface::class,
            RepaymentCalculatorService::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }
}
