<?php


namespace App\Http\Services;

use App\DTO\RepaymentCalculatorDTO;

/**
 * Interface RepaymentCalculatorServiceInterface
 * @package App\Http\Services
 */
interface RepaymentCalculatorServiceInterface
{
    public function calculateRepayments(
        RepaymentCalculatorDTO $repaymentCalculatorDTO
    );
}
