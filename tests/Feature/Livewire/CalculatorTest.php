<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\Calculator;
use App\Http\Services\RepaymentCalculatorService;
use JetBrains\PhpStorm\ArrayShape;
use Livewire\Livewire;
use Mockery\MockInterface;
use Tests\TestCase;

class CalculatorTest extends TestCase
{
    /**
     * Tests if the components returns a 200 response code
     */
    public function testComponentRenders(): void
    {
        $component = Livewire::test(Calculator::class);
        $component->assertStatus(200);
    }

    /**
     * Tests that correct validation errors are thrown for data that is supposed to fail validations.
     * @dataProvider failedValidationDataProvider
     * @param array $inputs
     * @param array $rule
     */
    public function testFailedValidations(array $inputs, array $rule): void
    {
        Livewire::test(Calculator::class)
            ->set('loanAmount', $inputs['loanAmount'] ?? null)
            ->set('interestRate', $inputs['interestRate'] ?? null)
            ->set('termDuration', $inputs['termDuration'] ?? null)
            ->set('repaymentFrequency', $inputs['repaymentFrequency'] ?? null)
            ->call('calculateRepayments')
            ->assertHasErrors($rule);
    }

    /**
     * Tests if the frequencies are correctly updated on calling the method.
     * @dataProvider repaymentsFrequencyValidationDataProvider
     * @param string $repaymentsFrequency
     */
    public function testUpdateFrequency(string $repaymentsFrequency): void
    {
        Livewire::test(Calculator::class)
            ->call('updateRepaymentsFrequency', $repaymentsFrequency)
            ->assertSet('repaymentFrequency', $repaymentsFrequency);
    }

    /**
     * Tests that for all the valid data, the repayments calculator service is called.
     * @dataProvider validDataProvider
     * @param array $inputs
     */
    public function testIfRepaymentsCalculatorServiceIsCalledForValidData(array $inputs): void
    {
        $this->mock(RepaymentCalculatorService::class, function (MockInterface $mock) {
            $mock->shouldReceive('calculateRepayments')->times(4);
        });
        Livewire::test(Calculator::class)
            ->set('loanAmount', $inputs['loanAmount'])
            ->set('interestRate', $inputs['interestRate'])
            ->set('termDuration', $inputs['termDuration'])
            ->set('repaymentFrequency', $inputs['repaymentFrequency'])
            ->call('calculateRepayments');
    }

    /**
     * Data provider for valid data set.
     * @return \array[][]
     */
    #[ArrayShape([
        'Valid data set #1' => "array[]",
        'Valid data set #2' => "array[]",
        'Valid data set #3' => "array[]"
    ])] public function validDataProvider(): array
    {
        return [
            'Valid data set #1' => [
                'inputs' => [
                    'loanAmount' => 1400.00,
                    'interestRate' => 20.00,
                    'termDuration' => 1,
                    'repaymentFrequency' => 'monthly'
                ]
            ],
            'Valid data set #2' => [
                'inputs' => [
                    'loanAmount' => 1400.00,
                    'interestRate' => 2.30,
                    'termDuration' => 3,
                    'repaymentFrequency' => 'monthly'
                ]
            ],
            'Valid data set #3' => [
                'inputs' => [
                    'loanAmount' => 1400.00,
                    'interestRate' => 24.00,
                    'termDuration' => 5,
                    'repaymentFrequency' => 'monthly'
                ]
            ]
        ];
    }

    /**
     * Data provider to test repayments frequency validation.
     * @return \string[][]
     */
    public function repaymentsFrequencyValidationDataProvider(): array
    {
        return [
            ['monthly'],
            ['weekly'],
            ['yearly']
        ];
    }

    /**
     * Data provider for dataste that would fail validation.
     * @return array[]
     */
    #[ArrayShape([
        'negative value for borrowed amount' => "array",
        'negative value for interest rate' => "array",
        'zero value for borrowed amount' => "array",
        'zero value for interest rate' => "array",
        'required value for borrowed amount' => "array",
        'required value for interest rate' => "array"
    ])] public function failedValidationDataProvider(): array
    {
        return [
            'negative value for borrowed amount' => [
                'inputs' => [
                    'loanAmount' => -1400.00,
                    'interestRate' => 20.00,
                    'termDuration' => 1,
                    'repaymentFrequency' => 'monthly'
                ],
                'rule' => [ 'loanAmount' => 'gt:0']
            ],
            'negative value for interest rate' => [
                'inputs' => [
                    'loanAmount' => 1400.00,
                    'interestRate' => -2.30,
                    'termDuration' => 1,
                    'repaymentFrequency' => 'monthly'
                ],
                'rule' => ['interestRate' => 'gt:0']
            ],
            'zero value for borrowed amount' => [
                'inputs' => [
                    'loanAmount' => 0,
                    'interestRate' => 24.00,
                    'termDuration' => 1,
                    'repaymentFrequency' => 'monthly'
                ],
                'rule' => [ 'loanAmount' => 'gt:0']
            ],
            'zero value for interest rate' => [
                'inputs' => [
                    'loanAmount' => 1400.00,
                    'interestRate' => 0,
                    'termDuration' => 1,
                    'repaymentFrequency' => 'monthly'
                ],
                'rule' => ['interestRate' => 'gt:0']
            ]
        ];
    }
}
