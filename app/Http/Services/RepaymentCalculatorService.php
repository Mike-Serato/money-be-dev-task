<?php


namespace App\Http\Services;

use App\DTO\RepaymentCalculatorDTO;
use Carbon\Carbon;

class RepaymentCalculatorService implements RepaymentCalculatorServiceInterface
{
    /**
     *
     * @param RepaymentCalculatorDTO $repaymentCalculatorDTO
     * @return float
     * @throws \Exception
     */
    public function calculateRepayments(
        RepaymentCalculatorDTO $repaymentCalculatorDTO
    ): float {
        $todayDate = Carbon::now();
        $endDate =  Carbon::now()->addYears($repaymentCalculatorDTO->tenureInYears);

        # INSTEAD of assuming that there are 52 weeks in a year, I have used the following method just in case if the
        # tenure is huge, it might affect the total number of weeks because there are 52.1429 weeks in an year and
        # then there are leap years too.
        $numberOfWeeks = $todayDate->diffInWeeks($endDate);
        $numberOfFortnights = $numberOfWeeks/2;

        # FOR uniformity sake, using Carbon's method to calculate number of months.
        $numberOfMonths = $todayDate->diffInMonths($endDate);

        # THE simple interest calculator formula is used here i.e.
        # A = P + P*R*T/100
        # WHERE A = Total payable amount at the end of loan tenure.
        # P = Principal amount
        # R = Rate of Interest
        # T = Tenure in years
        # To further calculate repayments based on frequency, I have divided the total amount with number of
        # repayments calculated above.
        $result = match ($repaymentCalculatorDTO->repaymentFrequency->value) {
            'monthly' => ($repaymentCalculatorDTO->borrowedAmount + ($repaymentCalculatorDTO->borrowedAmount *
                        $repaymentCalculatorDTO->interestRate *
                        $repaymentCalculatorDTO->tenureInYears / 100)) / $numberOfMonths,
            'fortnightly' => ($repaymentCalculatorDTO->borrowedAmount + ($repaymentCalculatorDTO->borrowedAmount *
                        $repaymentCalculatorDTO->interestRate *
                        $repaymentCalculatorDTO->tenureInYears / 100)) / $numberOfFortnights,
            'weekly' => ($repaymentCalculatorDTO->borrowedAmount + ($repaymentCalculatorDTO->borrowedAmount *
                        $repaymentCalculatorDTO->interestRate *
                        $repaymentCalculatorDTO->tenureInYears / 100)) / $numberOfWeeks
        };
        return round($result, 2);
    }
}
